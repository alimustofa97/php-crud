<!DOCTYPE html>
<html lang="en">
	<head>
    	<meta charset="utf-8">
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<!-- Popper JS -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
		<!-- Latest compiled JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  	</head>
  	<?php
  		require_once ("config.php");

	  	// Fetch all users data from database
		$filter = array();
		$filterQuery = '';
		$nama = '';
		$email = '';
		if (!empty($_GET['nama'])) {
			$nama = $_GET['nama'];
			array_push($filter, "nama LIKE '%$nama%'");
		}

		if (!empty($_GET['email'])) {
			$email = $_GET['email'];
			array_push($filter, "email LIKE '%$email%'");
		}

		if (!empty($filter)) {
			$filterQuery = "WHERE ".join("AND ", $filter);
		}
		  
		$result = mysqli_query($mysqli, "SELECT * FROM user $filterQuery ORDER BY id DESC");
		// $result = mysqli_query($mysqli, "SELECT * FROM user ORDER BY id DESC");
  	?>
  	<body class="p-3">
	  	<div class="container">
	  		<div class="row">
				<div class="col-md-4">
					<div class="card">
						<div class="card-header">
							Form
						</div>
						<div class="card-body">
							<form method="post" action="action.php?action=add" enctype="multipart/form-data">
								<div class="form-group">
									<label for="nama">Nama</label>
									<input type="nama" class="form-control" name="nama" id="nama" required>
								</div>
								<div class="form-group">
									<label for="alamat">Alamat</label>
									<input type="alamat" class="form-control"  name="alamat" id="alamat" required>
								</div>
								<div class="form-group">
									<label for="email">Email</label>
									<input type="email" class="form-control" name="email" id="email" required>
								</div>
								<div class="form-group">
									<label for="foto">Foto</label>
									<input type="file" class="form-control" id="foto" name="foto">
								</div>
								<button type="submit" class="btn btn-primary" name="submit" id="submit">Submit</button>
							</form>
						</div>
					</div>
	  			</div>
	  			<div class="col-md-8">
				  	<form method="get" class="mb-3">
						<div class="form-group">
							<label for="nama">Nama</label>
							<input type="nama" class="form-control" name="nama" value="<?php echo $nama; ?>" id="nama">
						</div>
						<div class="form-group">
							<label for="email">Email</label>
							<input type="email" class="form-control" name="email" value="<?php echo $email; ?>" id="email">
						</div>
						<button type="submit" class="btn btn-primary" id="submit">Cari</button>
					</form>
	  				<table class="table table-stripped">
	  					<thead>
						    <tr>
						        <th width="15%">Name</th>
						        <th width="15%">Alamat</th>
						        <th width="25%">Email</th>
						        <th width="25%">Foto</th>
						        <th width="25%">Action</th>
						    </tr>
						</thead>
						<tbody>
						    <?php  

						    $total = mysqli_num_rows($result);
						    if ($total == 0) {
						        echo "<tr>";
						        echo "<td align='center' colspan='5'>Empty</td>";
						        echo "</tr>";
						    } else {
						        while($user_data = mysqli_fetch_array($result)) {         
						            echo "<tr>";
						            echo "<td>".$user_data['nama']."</td>";
						            echo "<td>".$user_data['alamat']."</td>";
						            echo "<td>".$user_data['email']."</td>";      
						            echo "<td><img src='file/$user_data[foto]' width='100px'></td>";      
						            echo "<td><a href='update.php?id=$user_data[id]'>Edit</a>&emsp;";
						            echo "<a href='#' onclick='del($user_data[id])'>Delete</a></td>";      
						            echo "</tr>";
						        }
						    }

						    ?>
						</tbody>
					</table>
	  			</div>
	  		</div>
	  	</div>
	</body>
	<script>
	function del(id) {
		var result = confirm("Want to delete?");
		if (result) {
			location.href = "action.php?action=delete&id=" + id;
		}
	}
	</script>
</html>