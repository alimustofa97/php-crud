<?php
// include database connection file
include_once("config.php");
 
// Check if form is submitted for user update, then redirect to homepage after update
if(isset($_POST['update']))
{	
	$id = $_POST['id'];
	
	$nama=$_POST['nama'];
	$alamat=$_POST['alamat'];
	$email=$_POST['email'];
		
	// update user data
	$result = mysqli_query($mysqli, "UPDATE user SET nama='$nama',alamat='$alamat',email='$email' WHERE id=$id");
	
	// Redirect to homepage to display updated user in list
	header("Location: index.php");
}
?>
<?php
// Display selected user data based on id
// Getting id from url
$id = $_GET['id'];
 
// Fetech user data based on id
$result = mysqli_query($mysqli, "SELECT * FROM user WHERE id=$id");
 
while($user_data = mysqli_fetch_array($result))
{
	$nama = $user_data['nama'];
	$alamat = $user_data['alamat'];
	$email = $user_data['email'];
}
?>
<html>
<head>	
	<title>Edit User Data</title>
	<head>
    	<meta charset="utf-8">
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<!-- Popper JS -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
		<!-- Latest compiled JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  	</head>
</head>
<body class="p-3">
  	<div class="container">
  		<div class="row">
			<div class="col-md-6">
				<div class="card">
					<div class="card-header">
						Form
					</div>
					<div class="card-body">
						<form method="post" action="action.php?action=update">
							<input type="hidden" name="id" value="<?php echo $id; ?>">
							<div class="form-group">
								<label for="nama">Nama</label>
								<input type="nama" class="form-control" name="nama" id="nama" value="<?php echo $nama;?>">
							</div>
							<div class="form-group">
								<label for="alamat">Alamat</label>
								<input type="alamat" class="form-control"  name="alamat" id="alamat" value="<?php echo $alamat;?>">
							</div>
							<div class="form-group">
								<label for="email">Email</label>
								<input type="email" class="form-control" name="email" id="email" value="<?php echo $email;?>">
							</div>
							<button type="submit" class="btn btn-primary" name="submit" id="submit">Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>